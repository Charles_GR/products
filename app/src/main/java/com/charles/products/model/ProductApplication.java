package com.charles.products.model;

import android.app.Application;

import com.charles.products.model.data.ImageCache;

public final class ProductApplication extends Application
{
    private static ProductApplication instance;

    private ImageCache imageCache;

    public static ProductApplication getInstance()
    {
        return instance;
    }

    public ImageCache getImageCache()
    {
        return imageCache;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        instance = this;
        imageCache = new ImageCache();
    }
}
