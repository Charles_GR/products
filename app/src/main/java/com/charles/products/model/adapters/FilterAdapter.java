package com.charles.products.model.adapters;

import static java.util.Collections.unmodifiableList;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class FilterAdapter<Item, SortField extends Enum, SearchField extends Enum> extends ArrayAdapter<Item>
{
    protected final List<Item> items;
    protected final List<Item> filteredItems;
    protected ItemComparator comparator;

    protected FilterAdapter(Context context, int resource, List<Item> items)
    {
        super(context, resource, items);
        this.items = unmodifiableList(new ArrayList<>(items));
        this.filteredItems = items;
    }

    protected void setComparator(ItemComparator comparator)
    {
        this.comparator = comparator;
    }

    public void sortItems(SortField sortField, SortOrdering sortOrdering)
    {
        comparator.setSortField(sortField);
        comparator.setSortOrdering(sortOrdering);
        filteredItems.sort(comparator);
        notifyDataSetChanged();
    }

    public void searchItems(SearchField searchField, String searchQuery)
    {
        filteredItems.clear();
        if(searchQuery.isEmpty())
        {
            filteredItems.addAll(items);
        }
        else
        {
            performSearch(searchField, searchQuery);
        }
        notifyDataSetChanged();
    }

    protected abstract void performSearch(SearchField searchField, String searchQuery);

    public enum SortOrdering
    {
        Ascending, Descending
    }

    protected abstract class ItemComparator implements Comparator<Item>
    {
        protected SortField sortField;
        protected SortOrdering sortOrdering;

        protected void setSortField(SortField sortField)
        {
            this.sortField = sortField;
        }

        protected void setSortOrdering(SortOrdering sortOrdering)
        {
            this.sortOrdering = sortOrdering;
        }
    }
}

