package com.charles.products.model.adapters;

import static com.charles.products.model.adapters.ProductAdapter.SearchField;
import static com.charles.products.model.adapters.ProductAdapter.SortField;
import static com.charles.products.model.adapters.FilterAdapter.SortOrdering.Ascending;
import static java.lang.String.format;
import static java.util.Locale.UK;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.charles.products.R;
import com.charles.products.model.data.Product;
import com.charles.products.processing.tasks.FetchImageTask;
import com.charles.products.processing.util.ImageFetcher;

public final class ProductAdapter extends FilterAdapter<Product, SortField, SearchField>
{
    public ProductAdapter(Context context)
    {
        this(context, new ArrayList<>());
    }

    public ProductAdapter(Context context, List<Product> products)
    {
        super(context, R.layout.list_item_product, products);
        setComparator(new ProductComparator());
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_product, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvName = convertView.findViewById(R.id.tvName);
            viewHolder.tvPrice = convertView.findViewById(R.id.tvPrice);
            viewHolder.ivImage = convertView.findViewById(R.id.ivImage);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Product product = getItem(position);
        viewHolder.tvName.setText(product.getName());
        viewHolder.tvPrice.setText(format(UK, "£%d", product.getPrice()));
        new FetchImageTask(product.getId(), viewHolder.ivImage, new ImageFetcher(), null).execute();

        return convertView;
    }

    @Override
    protected void performSearch(SearchField searchField, String searchQuery)
    {
        items.stream().filter(product -> product.getName().contains(searchQuery)).forEach(filteredItems::add);
    }

    public enum SortField
    {
        Name, Price
    }

    public enum SearchField
    {
        Name
    }

    private final class ProductComparator extends ItemComparator
    {
        @Override
        public int compare(Product productA, Product productB)
        {
            int compare = 0;
            switch(sortField)
            {
                case Name:
                    compare = productA.getName().compareTo(productB.getName());
                    break;
                case Price:
                    compare = Double.compare(productA.getPrice(), productB.getPrice());
                    break;
            }
            return sortOrdering.equals(Ascending) ? compare : -1 * compare;
        }
    }

    private static final class ViewHolder
    {
        private TextView tvName;
        private TextView tvPrice;
        private ImageView ivImage;
    }
}