package com.charles.products.model.data;

import android.graphics.Bitmap;

import java.lang.ref.SoftReference;
import java.util.LinkedHashMap;

public final class ImageCache
{
    private static final int BYTES_PER_MEGABYTE = 1048576;
    private static final double LOAD_FACTOR = 0.75;
    private static final double MAX_IMAGE_SIZE_IN_MEGABYTES = 0.3;

    private final ImageCacheMap images;

    public ImageCache()
    {
        int capacity = (int) (calcAvailableCacheMemoryInMegabytes() / MAX_IMAGE_SIZE_IN_MEGABYTES);
        this.images = new ImageCacheMap(capacity);
    }

    public Bitmap getImage(String url)
    {
        SoftReference<Bitmap> bitmapRef = images.get(url);
        return bitmapRef == null ? null : bitmapRef.get();
    }

    public void addImage(String key, Bitmap bitmap)
    {
        if(bitmap == null)
        {
            return;
        }

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, false);
        double imageSize = (double) scaledBitmap.getByteCount() / BYTES_PER_MEGABYTE;
        if(imageSize <= calcAvailableCacheMemoryInMegabytes() && imageSize <= MAX_IMAGE_SIZE_IN_MEGABYTES)
        {
            images.put(key, new SoftReference<>(scaledBitmap));
        }
    }

    public void clearImages()
    {
        images.clear();
    }

    private static int calcAvailableCacheMemoryInMegabytes()
    {
        Runtime runtime = Runtime.getRuntime();
        long usedMemInMegabytes = (runtime.totalMemory() - runtime.freeMemory()) / BYTES_PER_MEGABYTE;
        long maxMemInMegabytes = runtime.maxMemory() / BYTES_PER_MEGABYTE;
        return (int) (LOAD_FACTOR * (maxMemInMegabytes - usedMemInMegabytes));
    }

    private static final class ImageCacheMap extends LinkedHashMap<String, SoftReference<Bitmap>>
    {
        private final int capacity;

        private ImageCacheMap(int capacity)
        {
            this.capacity = capacity;
        }

        @Override
        protected boolean removeEldestEntry(Entry<String, SoftReference<Bitmap>> eldest)
        {
            return size() > capacity;
        }
    }
}
