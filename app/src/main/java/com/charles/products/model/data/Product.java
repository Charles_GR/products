package com.charles.products.model.data;

import java.util.Objects;

import com.google.gson.annotations.SerializedName;

public final class Product
{
    @SerializedName("prodid") private final String id;
    private final String name;
    @SerializedName("cost") private final int price;

    public Product(String id, String name, int price)
    {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public int getPrice()
    {
        return price;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == this)
        {
            return true;
        }
        if(!(obj instanceof Product))
        {
            return false;
        }
        Product other = (Product) obj;
        return Objects.equals(id, other.id) && Objects.equals(name, other.name) &&
                Objects.equals(price, other.price);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, name, price);
    }
}
