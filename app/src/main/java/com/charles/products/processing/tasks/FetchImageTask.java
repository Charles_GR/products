package com.charles.products.processing.tasks;

import static java.lang.String.format;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.lang.ref.WeakReference;

import com.charles.products.R;
import com.charles.products.model.ProductApplication;
import com.charles.products.processing.util.ImageFetcher;

public final class FetchImageTask extends AsyncTask<Void, Void, Bitmap>
{
    private static final String IMAGE_URL = "http://riverisland.scene7.com/is/image/RiverIsland/%s_main";
    private static final Bitmap LOADING = BitmapFactory.decodeResource(ProductApplication.getInstance().getResources(), R.mipmap.loading);

    private final String productId;
    private final WeakReference<ImageView> imageViewRef;
    private final ImageFetcher imageFetcher;
    private final TaskListener taskListener;

    public FetchImageTask(String productId, ImageView imageView, ImageFetcher imageFetcher, TaskListener taskListener)
    {
        this.productId = productId;
        this.imageViewRef = new WeakReference<>(imageView);
        this.imageFetcher = imageFetcher;
        this.taskListener = taskListener;
    }

    @Override
    protected void onPreExecute()
    {
        if(taskListener != null)
        {
            taskListener.onTaskStarted();
        }
        setImageBitmap(LOADING);
    }

    @Override
    protected Bitmap doInBackground(Void... voids)
    {
        try
        {
            Bitmap bitmap = ProductApplication.getInstance().getImageCache().getImage(productId);
            if(bitmap == null)
            {
                bitmap = imageFetcher.fetchImage(format(IMAGE_URL, productId));
                ProductApplication.getInstance().getImageCache().addImage(productId, bitmap);
            }
            return bitmap;
        }
        catch(IOException e)
        {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap)
    {
        setImageBitmap(bitmap);
        if(taskListener != null)
        {
            taskListener.onTaskFinished();
        }
    }

    private void setImageBitmap(Bitmap bitmap)
    {
        ImageView imageView = imageViewRef.get();
        if(imageView != null)
        {
            imageView.setImageBitmap(bitmap);
        }
    }
}
