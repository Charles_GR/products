package com.charles.products.processing.tasks;

import static java.util.Collections.emptyList;

import android.os.AsyncTask;

import java.io.IOException;
import java.util.List;

import com.charles.products.model.data.Product;
import com.charles.products.model.runnable.ListRunnable;
import com.charles.products.processing.util.ProductParser;
import com.charles.products.processing.util.URLReader;

public final class FetchProductsTask extends AsyncTask<Void, Void, List<Product>>
{
    private static final String JSON_URL = "https://static.ristack.nn4maws.net/category/2506/products.json";

    private final URLReader urlReader;
    private final ProductParser productParser;
    private final ListRunnable<Product> onSuccess;
    private final Runnable onError;
    private final TaskListener taskListener;
    private boolean success;

    public FetchProductsTask(URLReader urlReader, ProductParser productParser, ListRunnable<Product> onSuccess,
                             Runnable onError, TaskListener taskListener)
    {
        this.urlReader = urlReader;
        this.productParser = productParser;
        this.onSuccess = onSuccess;
        this.onError = onError;
        this.taskListener = taskListener;
        this.success = true;
    }

    @Override
    protected void onPreExecute()
    {
        if(taskListener != null)
        {
            taskListener.onTaskStarted();
        }
    }

    @Override
    protected List<Product> doInBackground(Void... voids)
    {
        try
        {
            String json = urlReader.readAsString(JSON_URL);
            return productParser.parseProducts(json);
        }
        catch(IOException e)
        {
            success = false;
            return emptyList();
        }
    }

    @Override
    protected void onPostExecute(List<Product> products)
    {
        if(taskListener != null)
        {
            taskListener.onTaskFinished();
        }

        if(success)
        {
            onSuccess.run(products);
        }
        else
        {
            onError.run();
        }
    }
}
