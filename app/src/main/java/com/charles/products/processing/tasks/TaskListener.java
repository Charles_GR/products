package com.charles.products.processing.tasks;

public interface TaskListener
{
    void onTaskStarted();

    void onTaskFinished();
}
