package com.charles.products.processing.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public final class ImageFetcher
{
    public Bitmap fetchImage(String url) throws IOException
    {
        try(InputStream inputStream = new URL(url).openStream())
        {
            return BitmapFactory.decodeStream(inputStream);
        }
    }
}
