package com.charles.products.processing.util;

import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import com.charles.products.model.data.Product;

public final class ProductParser
{
    private static final Gson GSON = new Gson();

    public List<Product> parseProducts(String json)
    {
        JsonArray products = GSON.fromJson(json, JsonObject.class).get("Products").getAsJsonArray();
        return stream(products.spliterator(), false)
                .map(product -> GSON.fromJson(product, Product.class))
                .collect(toList());
    }
}
