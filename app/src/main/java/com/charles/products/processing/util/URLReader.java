package com.charles.products.processing.util;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

public final class URLReader
{
    public String readAsString(String url) throws IOException
    {
        try(Scanner scanner = new Scanner(new URL(url).openStream(), UTF_8.name()))
        {
            scanner.useDelimiter("\\A");
            return scanner.hasNext() ? scanner.next() : "";
        }
    }
}
