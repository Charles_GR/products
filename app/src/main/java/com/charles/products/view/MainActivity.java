package com.charles.products.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.charles.products.R;

public final class MainActivity extends Activity implements OnClickListener
{
    private Button btnViewProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnViewProducts = findViewById(R.id.btnViewProducts);
        btnViewProducts.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        if(view.getId() == R.id.btnViewProducts)
        {
            startActivity(new Intent(this, ProductListActivity.class));
        }
    }
}
