package com.charles.products.view;

import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_SENSOR;
import static android.content.res.Configuration.ORIENTATION_PORTRAIT;
import static java.lang.String.format;
import static java.util.Locale.UK;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.charles.products.processing.tasks.TaskListener;
import com.google.gson.Gson;

import com.charles.products.R;
import com.charles.products.model.data.Product;
import com.charles.products.processing.tasks.FetchImageTask;
import com.charles.products.processing.util.ImageFetcher;

public final class ProductDetailsActivity extends Activity implements TaskListener
{
    private TextView tvName;
    private TextView tvPrice;
    private ImageView ivImage;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        tvName = findViewById(R.id.tvName);
        tvPrice = findViewById(R.id.tvPrice);
        ivImage = findViewById(R.id.ivImage);
        showProductDetails();
    }

    private void showProductDetails()
    {
        String productJson = getIntent().getStringExtra("product");
        Product product = new Gson().fromJson(productJson, Product.class);
        tvName.setText(product.getName());
        tvPrice.setText(format(UK, "£%d", product.getPrice()));
        new FetchImageTask(product.getId(), ivImage, new ImageFetcher(), this).execute();
    }

    @Override
    public void onTaskStarted()
    {
        setRequestedOrientation(getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT ?
                SCREEN_ORIENTATION_PORTRAIT : SCREEN_ORIENTATION_LANDSCAPE);
    }

    @Override
    public void onTaskFinished()
    {
        setRequestedOrientation(SCREEN_ORIENTATION_SENSOR);
    }
}
