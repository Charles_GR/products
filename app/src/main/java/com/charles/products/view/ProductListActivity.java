package com.charles.products.view;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;
import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_SENSOR;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static android.widget.Toast.LENGTH_SHORT;
import static com.charles.products.model.adapters.ProductAdapter.SortField;
import static com.charles.products.model.adapters.ProductAdapter.SearchField;
import static com.charles.products.model.adapters.FilterAdapter.SortOrdering;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import com.charles.products.R;
import com.charles.products.model.ProductApplication;
import com.charles.products.model.adapters.ProductAdapter;
import com.charles.products.model.data.Product;
import com.charles.products.model.runnable.ListRunnable;
import com.charles.products.processing.tasks.FetchProductsTask;
import com.charles.products.processing.tasks.TaskListener;
import com.charles.products.processing.util.ProductParser;
import com.charles.products.processing.util.URLReader;

public final class ProductListActivity extends ListActivity
        implements OnClickListener, OnLongClickListener, OnItemSelectedListener, TextWatcher, TaskListener
{
    private EditText etSearchQuery;
    private ProgressBar pbProgress;
    private Spinner spnSortField;
    private Spinner spnSortOrdering;
    private Spinner spnSearchField;
    private TextView tvErrorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        etSearchQuery = findViewById(R.id.etSearchQuery);
        pbProgress = findViewById(R.id.pbProgress);
        spnSortField = findViewById(R.id.spnSortField);
        spnSortOrdering = findViewById(R.id.spnSortOrdering);
        spnSearchField = findViewById(R.id.spnSearchField);
        tvErrorMessage = findViewById(R.id.tvErrorMessage);
        etSearchQuery.addTextChangedListener(this);
        spnSortField.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, SortField.values()));
        spnSortField.setOnItemSelectedListener(this);
        spnSortOrdering.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, SortOrdering.values()));
        spnSortOrdering.setOnItemSelectedListener(this);
        spnSearchField.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, SearchField.values()));
        spnSearchField.setOnItemSelectedListener(this);
        setupActionBarWithCustomView();
        refreshProducts();
    }

    @Override
    public void onClick(View view)
    {
        if(view.getId() == R.id.ivRefresh)
        {
            ProductApplication.getInstance().getImageCache().clearImages();
            refreshProducts();
        }
    }

    @Override
    public boolean onLongClick(View view)
    {
        if(view.getId() == R.id.ivRefresh)
        {
            Toast.makeText(this, R.string.refresh, LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    @Override
    protected void onListItemClick(ListView listView, View view, int position, long id)
    {
        Product selectedProduct = (Product) listView.getItemAtPosition(position);
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("product", new Gson().toJson(selectedProduct));
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
    {
        searchAndSortProducts();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {
        searchAndSortProducts();
    }

    @Override
    public void afterTextChanged(Editable editable)
    {

    }

    @Override
    public void onTaskStarted()
    {
        setRequestedOrientation(getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT ?
                SCREEN_ORIENTATION_PORTRAIT : SCREEN_ORIENTATION_LANDSCAPE);
    }

    @Override
    public void onTaskFinished()
    {
        setRequestedOrientation(SCREEN_ORIENTATION_SENSOR);
    }

    private void setupActionBarWithCustomView()
    {
        if(getActionBar() != null && getActionBar().getCustomView() == null)
        {
            View customView = View.inflate(this, R.layout.action_layout_main, null);
            TextView tvTitle = customView.findViewById(R.id.tvTitle);
            ImageView ivRefresh = customView.findViewById(R.id.ivRefresh);
            tvTitle.setText(R.string.app_name);
            ivRefresh.setOnClickListener(this);
            ivRefresh.setOnLongClickListener(this);
            getActionBar().setDisplayShowCustomEnabled(true);
            getActionBar().setCustomView(customView);
        }
    }

    private void searchAndSortProducts()
    {
        ProductAdapter productAdapter = (ProductAdapter) getListView().getAdapter();
        if(productAdapter != null)
        {
            searchProducts(productAdapter);
            sortProducts(productAdapter);
        }
    }

    private void searchProducts(ProductAdapter productAdapter)
    {
        SearchField searchField = SearchField.valueOf(spnSearchField.getSelectedItem().toString());
        String searchQuery = etSearchQuery.getText().toString();
        productAdapter.searchItems(searchField, searchQuery);
    }

    private void sortProducts(ProductAdapter productAdapter)
    {
        SortField sortField = SortField.valueOf(spnSortField.getSelectedItem().toString());
        SortOrdering sortOrdering = SortOrdering.valueOf(spnSortOrdering.getSelectedItem().toString());
        productAdapter.sortItems(sortField, sortOrdering);
    }

    private void refreshProducts()
    {
        getListView().setAdapter(new ProductAdapter(this));
        tvErrorMessage.setVisibility(GONE);
        pbProgress.setVisibility(VISIBLE);
        new FetchProductsTask(new URLReader(), new ProductParser(), fetchProductsOnSuccess, fetchProductsOnError, this).execute();
    }

    private final ListRunnable<Product> fetchProductsOnSuccess = products ->
    {
        getListView().setAdapter(new ProductAdapter(ProductListActivity.this, products));
        searchAndSortProducts();
        pbProgress.setVisibility(GONE);
    };

    private final Runnable fetchProductsOnError = () ->
    {
        pbProgress.setVisibility(GONE);
        tvErrorMessage.setVisibility(VISIBLE);
    };
}