package com.charles.products.model.adapters;

import static com.charles.products.model.adapters.FilterAdapter.SortOrdering.Ascending;
import static com.charles.products.model.adapters.FilterAdapter.SortOrdering.Descending;
import static com.charles.products.model.adapters.ProductAdapter.SearchField;
import static com.charles.products.model.adapters.ProductAdapter.SortField;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.spy;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.charles.products.model.data.Product;

public final class ProductAdapterTest
{
    private static final Product JACKET = new Product("jac", "Jacket", 30);
    private static final Product TROUSERS = new Product("tro", "Trousers", 20);
    private static final Product SHOES = new Product("sho", "Shoes", 50);

    private ProductAdapter testee;

    @Before
    public void setup()
    {
        testee = spy(new ProductAdapter(context(), products()));
        doNothing().when(testee).notifyDataSetChanged();
    }

    @Test
    public void shouldCorrectlySortProductsByAscendingName()
    {
        testee.sortItems(SortField.Name, Ascending);

        assertThat(testee.filteredItems).containsExactly(JACKET, SHOES, TROUSERS);
        verify(testee).notifyDataSetChanged();
    }

    @Test
    public void shouldCorrectlySortProductsByDescendingName()
    {
        testee.sortItems(SortField.Name, Descending);

        assertThat(testee.filteredItems).containsExactly(TROUSERS, SHOES, JACKET);
        verify(testee).notifyDataSetChanged();
    }

    @Test
    public void shouldCorrectlySortProductsByAscendingPrice()
    {
        testee.sortItems(SortField.Price, Ascending);

        assertThat(testee.filteredItems).containsExactly(TROUSERS, JACKET, SHOES);
        verify(testee).notifyDataSetChanged();
    }

    @Test
    public void shouldCorrectlySortProductsByDescendingPrice()
    {
        testee.sortItems(SortField.Price, Descending);

        assertThat(testee.filteredItems).containsExactly(SHOES, JACKET, TROUSERS);
        verify(testee).notifyDataSetChanged();
    }

    @Test
    public void shouldCorrectlySearchProductsByName()
    {
        testee.searchItems(SearchField.Name, "jac");
        assertThat(testee.filteredItems).isEmpty();

        testee.searchItems(SearchField.Name, "Jac");
        assertThat(testee.filteredItems).containsExactly(JACKET);

        testee.searchItems(SearchField.Name, "s");
        assertThat(testee.filteredItems).containsExactlyInAnyOrder(TROUSERS, SHOES);

        verify(testee, times(3)).notifyDataSetChanged();
    }

    private static Context context()
    {
        return mock(Context.class);
    }

    private static List<Product> products()
    {
        return new ArrayList<>(asList(JACKET, TROUSERS, SHOES));
    }
}
