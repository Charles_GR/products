package com.charles.products.model.data;

import static java.lang.Math.ceil;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.reflect.Whitebox.getInternalState;
import static org.powermock.reflect.Whitebox.setInternalState;

import android.graphics.Bitmap;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ImageCache.class, Bitmap.class})
public final class ImageCacheTest
{
    private static final int BYTES_PER_MEGABYTE = 1048576;

    private ImageCache testee = new ImageCache();

    @Test
    public void shouldCreateCacheOfCorrectCapacity() throws Exception
    {
        mockStatic(ImageCache.class);
        doReturn(10).when(ImageCache.class, "calcAvailableCacheMemoryInMegabytes");

        testee = new ImageCache();
        double maxImageSize = getInternalState(ImageCache.class, "MAX_IMAGE_SIZE_IN_MEGABYTES");
        int actual = getInternalState(getInternalState(testee, "images"), "capacity");
        int expected = (int) (10 / maxImageSize);

        assertEquals(expected, actual);
    }

    @Test
    public void shouldNotAddImageToCacheWhichExceedsMaximumAllowedSize()
    {
        double maxImageSize = getInternalState(ImageCache.class, "MAX_IMAGE_SIZE_IN_MEGABYTES");

        Bitmap expected = mock(Bitmap.class);
        when(expected.getByteCount()).thenReturn(1 + (int) ceil(maxImageSize * BYTES_PER_MEGABYTE));
        mockStatic(Bitmap.class);
        when(Bitmap.createScaledBitmap(any(), anyInt(), anyInt(), anyBoolean())).thenReturn(expected);

        testee.addImage("product", expected);
        Bitmap actual = testee.getImage("product");

        assertNull(actual);
    }

    @Test
    public void shouldNotAddImageToCacheWhichExceedsAvailableMemory() throws Exception
    {
        mockStatic(ImageCache.class);
        doReturn(10).when(ImageCache.class, "calcAvailableCacheMemoryInMegabytes");

        Bitmap expected = mock(Bitmap.class);
        when(expected.getByteCount()).thenReturn(1 + 10 * BYTES_PER_MEGABYTE);
        mockStatic(Bitmap.class);
        when(Bitmap.createScaledBitmap(any(), anyInt(), anyInt(), anyBoolean())).thenReturn(expected);

        testee.addImage("product", expected);
        Bitmap actual = testee.getImage("product");

        assertNull(actual);
    }

    @Test
    public void shouldAddImageToCacheThenFetchEntryFromCache()
    {
        Bitmap expected = mock(Bitmap.class);
        mockStatic(Bitmap.class);
        when(Bitmap.createScaledBitmap(any(), anyInt(), anyInt(), anyBoolean())).thenReturn(expected);

        testee.addImage("product", expected);
        Bitmap actual = testee.getImage("product");

        assertEquals(expected, actual);
    }

    @Test
    public void shouldNotCauseErrorWhenAddingNullImageToCache()
    {
        testee.addImage("product", null);
    }

    @Test
    public void shouldReturnNullWhenProductIdNotFoundInCache()
    {
        Bitmap expected = mock(Bitmap.class);
        mockStatic(Bitmap.class);
        when(Bitmap.createScaledBitmap(any(), anyInt(), anyInt(), anyBoolean())).thenReturn(expected);

        Bitmap actual = testee.getImage("product");

        assertNull(actual);
    }

    @Test
    public void shouldClearImagesFromCache()
    {
        Bitmap expected = mock(Bitmap.class);
        mockStatic(Bitmap.class);
        when(Bitmap.createScaledBitmap(any(), anyInt(), anyInt(), anyBoolean())).thenReturn(expected);

        testee.addImage("productA", expected);
        testee.addImage("productB", expected);
        testee.clearImages();

        Map<String, Bitmap> images = getInternalState(testee, "images");
        assertTrue(images.isEmpty());
    }

    @Test
    public void shouldRemoveEldestEntryWhenAddingNewImageAtFullCapacity()
    {
        mockStatic(Bitmap.class);
        when(Bitmap.createScaledBitmap(any(), anyInt(), anyInt(), anyBoolean())).thenAnswer(
                invocation -> invocation.getArguments()[0]);
        setInternalState(getInternalState(testee, "images"), "capacity", 3);

        testee.addImage("productA", mock(Bitmap.class));
        testee.addImage("productB", mock(Bitmap.class));
        testee.addImage("productC", mock(Bitmap.class));
        testee.addImage("productD", mock(Bitmap.class));

        Map<String, Bitmap> images = getInternalState(testee, "images");
        assertThat(images.keySet()).containsExactly("productB", "productC", "productD");
    }
}
