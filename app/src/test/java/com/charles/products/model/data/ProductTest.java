package com.charles.products.model.data;

import static org.junit.Assert.assertEquals;

import com.google.gson.Gson;

import nl.jqno.equalsverifier.EqualsVerifier;

import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

public final class ProductTest
{
    private Gson GSON = new Gson();

    @Test
    public void shouldCorrectlySerializeToJson() throws Exception
    {
        Product product = new Product("jac", "Jacket", 30);
        String expected = "{\"prodid\": \"jac\", \"name\": \"Jacket\", \"cost\": 30}";

        String actual = GSON.toJson(product);

        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    public void shouldCorrectlyDeserializeFromJson()
    {
        String json = "{\"prodid\": \"jac\", \"name\": \"Jacket\", \"cost\": 30}";
        Product expected = new Product("jac", "Jacket", 30);

        Product actual = GSON.fromJson(json, Product.class);

        assertEquals(expected, actual);
    }

    @Test
    public void shouldObeyEqualsAndHashCodeContract()
    {
        EqualsVerifier.forClass(Product.class).verify();
    }
}
