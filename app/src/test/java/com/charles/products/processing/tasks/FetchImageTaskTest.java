package com.charles.products.processing.tasks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.charles.products.R;
import com.charles.products.model.ProductApplication;
import com.charles.products.model.data.ImageCache;
import com.charles.products.processing.util.ImageFetcher;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ProductApplication.class, BitmapFactory.class})
public final class FetchImageTaskTest
{
    private static final String PRODUCT_ID = "productId";

    private FetchImageTask testee;
    @Mock private ImageView imageView;
    @Mock private ImageFetcher imageFetcher;
    @Mock private TaskListener taskListener;
    @Mock private ImageCache imageCache;
    @Mock private ProductApplication productApplication;
    @Mock private Resources resources;
    @Mock private Bitmap loading;

    @Before
    public void setup()
    {
        mockResources();
        when(productApplication.getImageCache()).thenReturn(imageCache);

        testee = new FetchImageTask(PRODUCT_ID, imageView, imageFetcher, taskListener);
    }

    @Test
    public void shouldSetLoadingBitmapOnImageViewOnPreExecute()
    {
        testee.onPreExecute();

        verify(imageView).setImageBitmap(loading);
    }

    @Test
    public void shouldCallTaskListenerStartOnPreExecuteWhenProvided()
    {
        testee.onPreExecute();

        verify(taskListener).onTaskStarted();
    }

    @Test
    public void shouldNotCauseErrorOnPreExecuteWhenTaskListenerNotProvided()
    {
        taskListener = null;
        testee.onPreExecute();
    }

    @Test
    public void shouldReturnBitmapFromCacheWhenFoundInCache()
    {
        Bitmap expected = mock(Bitmap.class);
        when(imageCache.getImage(PRODUCT_ID)).thenReturn(expected);

        Bitmap actual = testee.doInBackground();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnBitmapFromUrlWhenNotFoundInCache() throws Exception
    {
        Bitmap expected = mock(Bitmap.class);
        when(imageCache.getImage(PRODUCT_ID)).thenReturn(null);
        when(imageFetcher.fetchImage(any())).thenReturn(expected);

        Bitmap actual = testee.doInBackground();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldAddBitmapToCacheWhenNotFoundInCache() throws Exception
    {
        Bitmap expected = mock(Bitmap.class);
        when(imageCache.getImage(PRODUCT_ID)).thenReturn(null);
        when(imageFetcher.fetchImage(any())).thenReturn(expected);

        testee.doInBackground();

        verify(imageCache).addImage(PRODUCT_ID, expected);
    }

    @Test
    public void shouldReturnNullBitmapWhenIOExceptionThrown() throws Exception
    {
        when(imageCache.getImage(PRODUCT_ID)).thenReturn(null);
        when(imageFetcher.fetchImage(any())).thenThrow(new IOException());

        Bitmap actual = testee.doInBackground();

        assertNull(actual);
    }

    @Test
    public void shouldSetFoundBitmapOnImageViewOnPostExecute()
    {
        Bitmap expected = mock(Bitmap.class);

        testee.onPostExecute(expected);

        verify(imageView).setImageBitmap(expected);
    }

    @Test
    public void shouldCallTaskListenerFinishOnPostExecute()
    {
        testee.onPostExecute(mock(Bitmap.class));

        verify(taskListener).onTaskFinished();
    }

    @Test
    public void shouldNotCauseErrorOnPostExecuteWhenTaskListenerNotProvided()
    {
        taskListener = null;
        testee.onPostExecute(mock(Bitmap.class));
    }

    private void mockResources()
    {
        mockStatic(ProductApplication.class);
        mockStatic(BitmapFactory.class);

        when(productApplication.getResources()).thenReturn(resources);
        when(ProductApplication.getInstance()).thenReturn(productApplication);
        when(BitmapFactory.decodeResource(resources, R.mipmap.loading)).thenReturn(loading);
    }
}
