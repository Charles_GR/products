package com.charles.products.processing.tasks;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.reflect.Whitebox.getInternalState;
import static org.powermock.reflect.Whitebox.setInternalState;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import com.charles.products.model.data.Product;
import com.charles.products.model.runnable.ListRunnable;
import com.charles.products.processing.util.ProductParser;
import com.charles.products.processing.util.URLReader;

@RunWith(PowerMockRunner.class)
public final class FetchProductsTaskTest
{
    private FetchProductsTask testee;
    @Mock private URLReader urlReader;
    @Mock private ProductParser productParser;
    @Mock private ListRunnable<Product> onSuccess;
    @Mock private Runnable onError;
    @Mock private TaskListener taskListener;

    @Before
    public void setup()
    {
        testee = new FetchProductsTask(urlReader, productParser, onSuccess, onError, taskListener);
    }

    @Test
    public void shouldCallTaskListenerStartOnPreExecuteWhenProvided()
    {
        testee.onPreExecute();

        verify(taskListener).onTaskStarted();
    }

    @Test
    public void shouldNotCauseErrorOnPreExecuteWhenTaskListenerNotProvided()
    {
        taskListener = null;
        testee.onPreExecute();
    }

    @Test
    public void shouldReturnProductsAndSuccessFlagWhenParsingSuccessful() throws Exception
    {
        List<Product> expected = asList(
            new Product("jac", "Jacket", 30),
            new Product("tro", "Trousers", 20));

        when(urlReader.readAsString(any())).thenReturn("json");
        when(productParser.parseProducts("json")).thenReturn(expected);

        List<Product> actual = testee.doInBackground();

        assertEquals(expected, actual);
        assertTrue(getInternalState(testee, "success"));
    }

    @Test
    public void shouldReturnEmptyListAndFailureFlagWhenParsingFailed() throws Exception
    {
        when(urlReader.readAsString(any())).thenThrow(new IOException());

        List<Product> actual = testee.doInBackground();

        assertTrue(actual.isEmpty());
        assertFalse(getInternalState(testee, "success"));
    }

    @Test
    public void shouldCallTaskListenerFinishOnPostExecute()
    {
        testee.onPostExecute(emptyList());

        verify(taskListener).onTaskFinished();
    }

    @Test
    public void shouldNotCauseErrorOnPostExecuteWhenTaskListenerNotProvided()
    {
        taskListener = null;
        testee.onPostExecute(emptyList());
    }

    @Test
    public void shouldCallOnSuccessCallbackWithProductsWhenFinishedAndSuccessful()
    {
        List<Product> expected = emptyList();
        setInternalState(testee, "success", true);

        testee.onPostExecute(expected);

        verify(onSuccess).run(expected);
        verify(onError, never()).run();
    }

    @Test
    public void shouldCallOnErrorCallbackWhenFinishedAndFailed()
    {
        setInternalState(testee, "success", false);

        testee.onPostExecute(emptyList());

        verify(onSuccess, never()).run(any());
        verify(onError).run();
    }
}
