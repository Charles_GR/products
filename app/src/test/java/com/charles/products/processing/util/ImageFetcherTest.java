package com.charles.products.processing.util;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.InputStream;
import java.net.URL;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ImageFetcher.class, BitmapFactory.class})
public final class ImageFetcherTest
{
    private ImageFetcher testee = new ImageFetcher();
    @Mock private InputStream inputStream;
    @Mock private URL url;

    @Test
    public void shouldCorrectlyFetchImageFromUrl() throws Exception
    {
        when(url.openStream()).thenReturn(inputStream);
        whenNew(URL.class).withArguments("imageUrl").thenReturn(url);

        Bitmap expected = mock(Bitmap.class);
        mockStatic(BitmapFactory.class);
        when(BitmapFactory.decodeStream(inputStream)).thenReturn(expected);

        Bitmap actual = testee.fetchImage("imageUrl");

        assertEquals(expected, actual);
    }
}

