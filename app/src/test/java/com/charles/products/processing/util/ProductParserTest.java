package com.charles.products.processing.util;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.joining;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.junit.Test;

import com.charles.products.model.data.Product;

public final class ProductParserTest
{
    private ProductParser testee = new ProductParser();

    @Test
    public void shouldCorrectlyParseJsonToListOfProducts() throws Exception
    {
        String json = readFileFromClasspath("products.json");

        List<Product> actual = testee.parseProducts(json);

        assertThat(actual).containsExactly(
            new Product("jac", "Jacket", 30),
            new Product("tro", "Trousers", 20));
    }

    @SuppressWarnings("ConstantConditions")
    private static String readFileFromClasspath(String fileName) throws Exception
    {
        InputStream inputStream = ProductParserTest.class.getClassLoader().getResourceAsStream(fileName);
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, UTF_8)))
        {
            return reader.lines().collect(joining("\n"));
        }
    }
}
