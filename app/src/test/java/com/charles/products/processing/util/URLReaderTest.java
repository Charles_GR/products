package com.charles.products.processing.util;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.io.ByteArrayInputStream;
import java.net.URL;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(URLReader.class)
public final class URLReaderTest
{
    private URLReader testee = new URLReader();

    @Test
    public void shouldCorrectlyReadURLAsString() throws Exception
    {
        String expected = "test";
        URL url = mock(URL.class);
        when(url.openStream()).thenReturn(new ByteArrayInputStream(expected.getBytes(UTF_8)));
        whenNew(URL.class).withArguments("url").thenReturn(url);

        String actual = testee.readAsString("url");

        assertEquals(expected, actual);
    }
}
